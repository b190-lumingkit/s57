let collection = [];

function print()
{
    if (collection.length <= 0) 
    {
        return collection;
    } 
    else 
    {
        for (let index = 0; index < collection.length; index++) {
            const element = collection[index];
            return element;
        }
    }
}

function enqueue(name = 'John')
{
    const l = collection.length
    if (l <= 0 || l === 1)
    {
        collection[l] = name;
    }
    else
    {
        collection[l] = name;
    }
    return collection;
}

function dequeue()
{
    for (let index = 0; index < collection.length; index++) 
    {
        collection[index] = collection[index + 1];
        --collection.length      
    }
    return collection;
}

function front()
{
    return collection[0];
}

function size()
{
    return collection.length;
}

function isEmpty()
{
    if(collection[0])
    {
        return false;
    }
    else
    {
        return true;
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};